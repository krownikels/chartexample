﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class PenggunaInfo
    {
        public int ID { get; set; }
        public string Nama { get; set; }
        public string Jantina { get; set; }
        public DateTime Tarikh { get; set; }
    }
}
