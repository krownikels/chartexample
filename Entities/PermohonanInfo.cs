﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class PermohonanInfo
    {
        public int ID { get; set; }
        public string Kategory { get; set; }
        public string Status { get; set; }
        public DateTime Tarikh { get; set; }
    }
}
