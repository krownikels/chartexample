﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using Dapper;
using System.Linq;
using Entities;


namespace DataAccess
{
    public class DashboardDataService
    {
        private IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DashboardDbConnection"].ToString());
        
        public DashboardDataService()
        {
        }

        public List<PermohonanInfo> GetPermohonan()
        {
            try
            {
                var sql = "SELECT * FROM Permohonan";

                var results = this.db.Query<PermohonanInfo>(sql, null, commandType: CommandType.Text).ToList();

                return results;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<PenggunaInfo> GetPengguna()
        {
            try
            {
                var sql = "SELECT * FROM Pengguna";

                var results = this.db.Query<PenggunaInfo>(sql, null, commandType: CommandType.Text).ToList();

                return results;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        
    }


}
