﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;
using DataAccess;
using Entities;
using Microsoft.Ajax.Utilities;

namespace ChartExample.Controllers
{
    public class DashboardController : Controller
    {
        private DashboardDataService dServ;
        List<string> colorArray = new List<string>(new string[] { "#81C3D7", "#f4a261", "#e9c46a", "#06d6a0", "#7400b8", "#56cfe1" });
        public DashboardController()
        {
            this.dServ = new DashboardDataService();
        }

        // GET: Dashboard
        public ActionResult Index()
        {
            var permohonanData = GetPermohonan();
            var penggunaData = GetPengguna();

            ViewBag.PermohonanCount = permohonanData.Count;
            ViewBag.PenggunaCount = penggunaData.Count;
            return View();
        }

        public List<PermohonanInfo> GetPermohonan()
        {
            var permohonanData = this.dServ.GetPermohonan();

            if (permohonanData != null || permohonanData.Count > 0)
            {
                return permohonanData;
            }
            else
            {
                return null;
            }
        }

        public List<PenggunaInfo> GetPengguna()
        {
            var penggunaData = this.dServ.GetPengguna();
            //results.cou

            if (penggunaData != null || penggunaData.Count > 0)
            {
                return penggunaData;
            }
            else
            {
                return null;
            }

        }

        [HttpPost]
        public ActionResult PenggunaByJantina()
        {
            List<string> value = new List<string>();
            List<string> label = new List<string>();
            List<string> color = new List<string>();

            var penggunaData = this.dServ.GetPengguna();

            if (penggunaData != null || penggunaData.Count > 0)
            {
                var data = penggunaData
                   .GroupBy(p => p.Jantina).Select(group => new {
                       keys = group.Key,
                       count = group.Count()
                   }).ToList();

                for (int i = 0; i < data.Count; i++)
                {
                    label.Add(data[i].keys);
                    value.Add(data[i].count.ToString());
                    color.Add(colorArray[i]);
                }

                return Json(new { label, value, color });
            }
            else
            {
                return null;
            }

        }

        [HttpPost]
        public ActionResult PermohonanByKategori()
        {
            List<string> value = new List<string>();
            List<string> label= new List<string>();
            List<string> color = new List<string>();

            var permohonanData = this.dServ.GetPermohonan();

            if (permohonanData != null || permohonanData.Count > 0)
            {
                var data = permohonanData
                   .GroupBy(p => p.Kategory).Select(group => new {
                       keys = group.Key,
                       count = group.Count()
                   }).ToList();

                for (int i = 0; i < data.Count; i++)
                {
                    label.Add(data[i].keys);
                    value.Add(data[i].count.ToString());
                    color.Add(colorArray[i]);
                }

                return Json(new { label, value, color });
            }
            else
            {
                return null;
            }

        }

        [HttpPost]
        public ActionResult PermohonanKategoriByStatus()
        {
            List<string> valueFirst = new List<string>();
            List<string> valueSecond = new List<string>();
            List<string> mainlabel = new List<string>();
            List<string> nameFirst = new List<string>();
            List<string> nameSecond = new List<string>();
            List<string> colorFirst = new List<string>();
            List<string> colorSecond = new List<string>();

            var permohonanData = this.dServ.GetPermohonan();

            if (permohonanData != null || permohonanData.Count > 0)
            {
                var data = permohonanData;

                var lstLulus = data.Where(s => s.Status == "LULUS").GroupBy(s => s.Tarikh.Year).Select(group => new {
                    keys = group.Key,
                    count = group.Count()
                }).OrderBy(s => s.keys).ToList();
                
                var lstGagal = data.Where(s => s.Status == "GAGAL").GroupBy(s => s.Tarikh.Year).Select(group => new {
                    keys = group.Key,
                    count = group.Count()
                }).OrderBy(s => s.keys).ToList();


                for (int i = 0; i < lstGagal.Count; i++)
                {
                    mainlabel.Add(lstGagal[i].keys.ToString());
                    valueSecond.Add(lstGagal[i].count.ToString());
                }

                for (int i = 0; i < lstLulus.Count; i++)
                {
                    valueFirst.Add(lstLulus[i].count.ToString());
                }

                nameFirst.Add("LULUS");
                nameSecond.Add("GAGAL");
                colorFirst.Add(colorArray[0]);
                colorSecond.Add(colorArray[1]);

                return Json(new { mainlabel, valueFirst, valueSecond, nameFirst, nameSecond, colorFirst, colorSecond });
            }
            else
            {
                return null;
            }

        }

        [HttpPost]
        public ActionResult PenggunaByTarikh()
        {
            List<string> valueFirst = new List<string>();
            List<string> valueSecond = new List<string>();
            List<string> mainlabel = new List<string>();
            List<string> nameFirst = new List<string>();
            List<string> nameSecond = new List<string>();
            List<string> colorFirst = new List<string>();
            List<string> colorSecond = new List<string>();

            var penggunaData = this.dServ.GetPengguna();

            if (penggunaData != null || penggunaData.Count > 0)
            {
                var data = penggunaData;

                var lstLelaki = data.Where(s => s.Jantina == "Lelaki").GroupBy(s => s.Tarikh.Year).Select(group => new {
                    keys = group.Key,
                    count = group.Count()
                }).OrderBy(s => s.keys).ToList();

                var lstPerempuan = data.Where(s => s.Jantina == "Perempuan").GroupBy(s => s.Tarikh.Year).Select(group => new {
                    keys = group.Key,
                    count = group.Count()
                }).OrderBy(s => s.keys).ToList();


                for (int i = 0; i < lstPerempuan.Count; i++)
                {
                    valueSecond.Add(lstPerempuan[i].count.ToString());
                }

                for (int i = 0; i < lstLelaki.Count; i++)
                {
                    mainlabel.Add(lstLelaki[i].keys.ToString());
                    valueFirst.Add(lstLelaki[i].count.ToString());
                }

                nameFirst.Add("LELAKI");
                nameSecond.Add("PEREMPUAN");
                colorFirst.Add(colorArray[0]);
                colorSecond.Add(colorArray[1]);

                return Json(new { mainlabel, valueFirst, valueSecond, nameFirst, nameSecond, colorFirst, colorSecond });
            }
            else
            {
                return null;
            }

        }

    }
}