USE [DashboardDB]
GO
/****** Object:  Table [dbo].[Pengguna]    Script Date: 9/11/2020 1:49:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pengguna](
	[ID] [bigint] NULL,
	[Nama] [nvarchar](50) NULL,
	[Jantina] [nvarchar](50) NULL,
	[Tarikh] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permohonan]    Script Date: 9/11/2020 1:49:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permohonan](
	[ID] [bigint] NULL,
	[Kategory] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Tarikh] [datetime] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (1, N'Atikah', N'Perempuan', CAST(N'2020-05-28T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (2, N'Atirah', N'Perempuan', CAST(N'2017-07-19T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (3, N'Ali', N'Lelaki', CAST(N'2019-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (4, N'Aira', N'Perempuan', CAST(N'2016-11-16T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (5, N'Luqman', N'Lelaki', CAST(N'2019-12-13T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (6, N'Irfan', N'Lelaki', CAST(N'2018-03-13T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (7, N'Has', N'Perempuan', CAST(N'2019-01-20T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (8, N'Syikin', N'Perempuan', CAST(N'2019-12-31T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (9, N'Hidayah', N'Perempuan', CAST(N'2019-12-10T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (10, N'Amil', N'Lelaki', CAST(N'2019-03-12T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (11, N'Azma', N'Perempuan', CAST(N'2016-03-04T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (12, N'Aisyah', N'Perempuan', CAST(N'2020-01-28T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (13, N'Ikhwan', N'Lelaki', CAST(N'2017-02-11T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (14, N'Azam', N'Lelaki', CAST(N'2016-03-02T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (15, N'Aqilah', N'Perempuan', CAST(N'2017-11-10T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (16, N'Irfan', N'Lelaki', CAST(N'2019-09-23T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (17, N'Wahab', N'Lelaki', CAST(N'2018-02-02T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (18, N'Nurul', N'Perempuan', CAST(N'2020-04-21T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (19, N'Afrina', N'Perempuan', CAST(N'2019-11-27T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (20, N'Rizkin', N'Lelaki', CAST(N'2020-09-12T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (21, N'Azmir', N'Lelaki', CAST(N'2017-02-27T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (22, N'Faqihah', N'Perempuan', CAST(N'2016-05-08T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (23, N'Qistina', N'Perempuan', CAST(N'2019-07-22T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (24, N'Syahirah', N'Perempuan', CAST(N'2016-11-05T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (25, N'Anis', N'Perempuan', CAST(N'2017-05-08T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (26, N'Alisha', N'Perempuan', CAST(N'2016-01-29T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (27, N'Amir', N'Lelaki', CAST(N'2018-10-18T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (28, N'Haura', N'Perempuan', CAST(N'2018-05-09T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (29, N'Adam', N'Lelaki', CAST(N'2019-01-28T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (30, N'Zaidat', N'Perempuan', CAST(N'2017-09-03T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (31, N'Amira', N'Perempuan', CAST(N'2019-10-27T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (32, N'Nabilah', N'Perempuan', CAST(N'2020-05-06T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (33, N'Husin', N'Lelaki', CAST(N'2016-03-12T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (34, N'Filzah', N'Perempuan', CAST(N'2018-06-27T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (35, N'Rohamidun', N'Lelaki', CAST(N'2016-09-28T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (36, N'Erra', N'Perempuan', CAST(N'2017-12-09T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (37, N'Zana', N'Perempuan', CAST(N'2017-10-05T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (38, N'Ummi', N'Perempuan', CAST(N'2019-03-29T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (39, N'Zahirah', N'Perempuan', CAST(N'2016-06-01T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (40, N'Alia', N'Perempuan', CAST(N'2019-05-01T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (41, N'Blakeway', N'Lelaki', CAST(N'2015-09-17T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (42, N'Azam', N'Lelaki', CAST(N'2019-07-27T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (43, N'Sofia', N'Perempuan', CAST(N'2020-05-26T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (44, N'Wan', N'Lelaki', CAST(N'2020-11-01T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (45, N'Syed', N'Lelaki', CAST(N'2020-05-13T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (46, N'Iskandar', N'Lelaki', CAST(N'2018-08-24T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (47, N'Sarah', N'Perempuan', CAST(N'2018-04-16T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (48, N'Danial', N'Lelaki', CAST(N'2020-10-20T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (49, N'Iman', N'Lelaki', CAST(N'2018-03-24T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (50, N'Hana', N'Perempuan', CAST(N'2019-04-06T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (51, N'Arif', N'Lelaki', CAST(N'2019-06-10T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (52, N'Adra', N'Perempuan', CAST(N'2016-11-05T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (53, N'Raiz', N'Lelaki', CAST(N'2018-07-17T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (54, N'Syuhada', N'Perempuan', CAST(N'2020-06-19T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (55, N'Syahmi', N'Lelaki', CAST(N'2019-04-29T00:00:00.000' AS DateTime))
INSERT [dbo].[Pengguna] ([ID], [Nama], [Jantina], [Tarikh]) VALUES (56, N'Syarifah', N'Perempuan', CAST(N'2016-05-13T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (1, N'PENAMBAHAN', N'LULUS', CAST(N'2016-06-06T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (2, N'PERTUKARAN', N'GAGAL', CAST(N'2017-09-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (3, N'PEMBUANGAN', N'LULUS', CAST(N'2017-06-04T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (4, N'PERTUKARAN', N'PROSES', CAST(N'2019-11-18T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (5, N'PENAMBAHAN', N'GAGAL', CAST(N'2017-11-24T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (6, N'PENAMBAHAN', N'PROSES', CAST(N'2016-01-19T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (7, N'PERTUKARAN', N'LULUS', CAST(N'2017-08-13T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (8, N'PEMBUANGAN', N'LULUS', CAST(N'2018-01-22T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (9, N'PENAMBAHAN', N'PROSES', CAST(N'2015-09-12T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (10, N'PERTUKARAN', N'PROSES', CAST(N'2019-03-20T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (11, N'PEMBUANGAN', N'LULUS', CAST(N'2018-07-28T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (12, N'PERTUKARAN', N'GAGAL', CAST(N'2015-12-26T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (13, N'PENAMBAHAN', N'LULUS', CAST(N'2017-12-05T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (14, N'PEMBUANGAN', N'GAGAL', CAST(N'2019-12-21T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (15, N'PENAMBAHAN', N'LULUS', CAST(N'2016-10-20T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (16, N'PERTUKARAN', N'PROSES', CAST(N'2015-11-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (17, N'PENAMBAHAN', N'PROSES', CAST(N'2019-07-18T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (18, N'PEMBUANGAN', N'GAGAL', CAST(N'2016-10-20T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (19, N'PERTUKARAN', N'LULUS', CAST(N'2017-09-01T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (20, N'PEMBUANGAN', N'LULUS', CAST(N'2019-01-13T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (21, N'PENAMBAHAN', N'LULUS', CAST(N'2015-10-29T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (22, N'PERTUKARAN', N'GAGAL', CAST(N'2017-09-06T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (23, N'PEMBUANGAN', N'LULUS', CAST(N'2017-07-01T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (24, N'PERTUKARAN', N'PROSES', CAST(N'2015-09-18T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (25, N'PENAMBAHAN', N'PROSES', CAST(N'2019-10-26T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (26, N'PEMBUANGAN', N'LULUS', CAST(N'2019-11-18T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (27, N'PERTUKARAN', N'PROSES', CAST(N'2018-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (28, N'PENAMBAHAN', N'LULUS', CAST(N'2016-02-12T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (29, N'PEMBUANGAN', N'GAGAL', CAST(N'2018-10-11T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (30, N'PERTUKARAN', N'LULUS', CAST(N'2019-01-03T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (31, N'PENAMBAHAN', N'LULUS', CAST(N'2019-07-25T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (32, N'PENAMBAHAN', N'PROSES', CAST(N'2016-03-10T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (33, N'PERTUKARAN', N'PROSES', CAST(N'2019-04-10T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (34, N'PEMBUANGAN', N'GAGAL', CAST(N'2020-08-31T00:00:00.000' AS DateTime))
INSERT [dbo].[Permohonan] ([ID], [Kategory], [Status], [Tarikh]) VALUES (35, N'PERTUKARAN', N'LULUS', CAST(N'2016-08-19T00:00:00.000' AS DateTime))
GO
