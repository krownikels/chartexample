﻿var DashboardModule = (function () {

    var init = function () {
        loadPermohonanChart("pie");
        loadPenggunaChart("pie");
    };

    var loadPermohonanChart = function (type) {
        if (type == "pie") {
            $.when(GetPermohonanByKategori()).then(function (results) {
                loadPieChart("#myChart1", results);
            });
        }
        else if (type == "radar") {
            $.when(GetPermohonanKategoriByStatus()).then(function (results) {
                loadRadarChart("#myChart1", results);
            });
        }
        else if (type == "doughnut") {
            $.when(GetPermohonanByKategori()).then(function (results) {
                loadDoughnutChart("#myChart1", results);
            });
        }
        else if (type == "bar") {
            $.when(GetPermohonanByKategori()).then(function (results) {
                loadBarChart("#myChart1", results);
            });
        }
        else if (type == "line") {
            $.when(GetPermohonanKategoriByStatus()).then(function (results) {
                loadLineChart("#myChart1", results);
            });
        }
    };

    var loadPenggunaChart = function (type) {
        if (type == "pie") {
            $.when(GetPenggunaByGender()).then(function (results) {
                loadPieChart("#myChart2", results);
            });
        }
        else if (type == "radar") {
            $.when(GetPenggunaByTarikh()).then(function (results) {
                loadRadarChart("#myChart2", results);
            });
        }
        else if (type == "doughnut") {
            $.when(GetPenggunaByGender()).then(function (results) {
                loadDoughnutChart("#myChart2", results);
            });
        }
        else if (type == "bar") {
            $.when(GetPenggunaByGender()).then(function (results) {
                loadBarChart("#myChart2", results);
            });
        }
        else if (type == "line") {
            $.when(GetPenggunaByTarikh()).then(function (results) {
                console.log(results);
                loadLineChart("#myChart2", results);
            });
        }
    };


    var loadPieChart = function (value, dataList) {      
        var ctx = $(value);
        new Chart(ctx, {
            type: 'pie',
            data: {
                labels: dataList.label,
                datasets: [{
                    backgroundColor: dataList.color,
                    data: dataList.value
                }]
            }
        });
    };

    var loadRadarChart = function (value, dataList) {      
        var ctx = $(value);
        new Chart(ctx, {
            type: 'radar',
            data: {
                labels: dataList.mainlabel,
                datasets: [
                    {
                        label: dataList.nameFirst,
                        fill: false,
                        borderColor: dataList.colorFirst,
                        data: dataList.valueFirst
                    },
                    {
                        label: dataList.nameSecond,
                        fill: false,
                        borderColor: dataList.colorSecond,
                        data: dataList.valueSecond
                    }
                ]
            }
        });
    };

    var loadDoughnutChart = function (value, dataList) {      
        var ctx = $(value);
        new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: dataList.label,
                datasets: [{
                    backgroundColor: dataList.color,
                    data: dataList.value
                }]
            }
        });
    };

    var loadBarChart = function (value, dataList) {      
        var ctx = $(value);
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: dataList.label,
                datasets: [{
                    backgroundColor: dataList.color,
                    data: dataList.value
                }]
            },

            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                legend: {
                    display: false
                }
            }
        });
    };

    var loadLineChart = function (value, dataList) {   
        var ctx = $(value);
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: dataList.mainlabel,
                datasets: [
                    {
                        label: dataList.nameFirst,
                        fill: false,
                        borderColor: dataList.colorFirst,
                        data: dataList.valueFirst
                    },
                    {
                        label: dataList.nameSecond,
                        fill: false,
                        borderColor: dataList.colorSecond,
                        data: dataList.valueSecond
                    }
                ]
            }
        });
    };

    var GetPenggunaByGender = function () {
        var d = new $.Deferred();

        var uri = "Dashboard/PenggunaByJantina";
        $.post(uri).done(function (returnData) {
            d.resolve(returnData);
        });

        return d.promise();
    };

    var GetPermohonanByKategori = function () {
        var d = new $.Deferred();

        var uri = "Dashboard/PermohonanByKategori";
        $.post(uri).done(function (returnData) {
            d.resolve(returnData);
        });

        return d.promise();
    };

    var GetPermohonanKategoriByStatus = function () {
        var d = new $.Deferred();

        var uri = "Dashboard/PermohonanKategoriByStatus";
        $.post(uri).done(function (returnData) {
            d.resolve(returnData);
        });

        return d.promise();
    };

    var GetPenggunaByTarikh = function () {
        var d = new $.Deferred();

        var uri = "Dashboard/PenggunaByTarikh";
        $.post(uri).done(function (returnData) {
            d.resolve(returnData);
        });

        return d.promise();
    };

    var resetChart1 = function () {
        $("#chartContainer1").html("");
        $("#chartContainer1").html("<canvas id='myChart1' style='width: 100 %;'></canvas>");
    };

    var resetChart2 = function () {
        $("#chartContainer2").html("");
        $("#chartContainer2").html("<canvas id='myChart2' style='width: 100 %;'></canvas>");
    };

    return {
        init: init,
        loadPermohonanChart: loadPermohonanChart,
        loadPenggunaChart: loadPenggunaChart,
        resetChart1: resetChart1,
        resetChart2: resetChart2
    };
})();